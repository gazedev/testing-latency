# Modify nginx.conf to listen for the right server_name if you need to
FOLDER="test2"
DOMAIN="test2.pittsburghhousing.org"


# Copy our config to the nginx sites-available directory, with a more specific name
sudo cp nginx.conf /etc/nginx/sites-available/${FOLDER}

# Symlink pgh_geocode_api nginx config to sites-enabled to enable it
sudo ln -sf /etc/nginx/sites-available/${FOLDER} /etc/nginx/sites-enabled

# To check for typos in your file:
sudo nginx -t

# If you get no errors, you can restart nginx:
sudo service nginx restart

sudo certbot --nginx -d ${DOMAIN}
# Enter email address...:
# (A)gree to Terms
# (N)o sharing of email address
# 2 - Redirect all requests to https

# To check for typos in your file:
sudo nginx -t

# If you get no errors, you can restart nginx to apply the changes:
sudo service nginx restart