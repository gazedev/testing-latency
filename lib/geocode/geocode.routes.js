const Joi = require('@hapi/joi');

const iterationsInput = Joi.object().keys({
  i: Joi.number().required(),
})

module.exports = {
  routes: (models) => {
    const controllers = require('./geocode.controllers')(models);
    return [
      {
        method: 'GET',
        path: '/status',
        handler: controllers.getStatusHttp,
        config: {
          description: 'Get status',
          notes: 'Just returns a status',
          tags: ['api', 'Status'],
        }
      },
      {
        method: 'GET',
        path: '/status/request/http',
        handler: controllers.requestStatusHttp,
        config: {
          description: 'Request status via http',
          notes: 'Requests a status',
          tags: ['api', 'Status'],
          validate: {
            query: iterationsInput,
          }
        }
      },




      {
        method: 'POST',
        path: '/ws/status',
        handler: controllers.getStatusWebsocket,
        config: {
          payload: { output: "data", parse: true, allow: "application/json" },
          plugins: {
            // plugins: { websocket: { only: true, autoping: 30 * 1000 } }
            // websocket: true,
            websocket: {
              only: true,
              autoping: 10 * 1000,
              // subprotocol: "quux.example.com",
              initially: true,
              // connect: ({ ctx, wss, ws, req, peers }) => {
              //     console.log("connect");
              //     ws.send(JSON.stringify({ cmd: "HELLO", arg: "CONNECT" }));
              // },
              // disconnect: ({ ctx, wss, ws, req, peers }) => {
              //     console.log("disconnect");
              // }
            }
          },
          // pre: [
          //   { method: (request, h) => {
          //     console.log("pre");
          //     return h.continue;
          //   } }
          // ],
          description: 'Get status via websocket',
          notes: 'Just returns a status',
          tags: ['api', 'WebSockets'],
        }
      },
      {
        method: 'GET',
        path: '/status/request/websocket',
        handler: controllers.requestStatusWebsocket,
        config: {
          description: 'Request status via websocket',
          notes: 'Requests a status',
          tags: ['api', 'WebSockets'],
          validate: {
            query: iterationsInput,
          }
        },
      },
      
    ];
  },
};
