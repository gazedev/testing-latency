const WebSocket = require('ws');
const { httpGet } = require('../helpers/helpers');

module.exports = (models) => {

  // const used = process.memoryUsage().heapUsed / 1024 / 1024;
  // console.log(`The script uses approximately ${Math.round(used * 100) / 100} MB`);

  return {
    getStatusHttp: async function(request, h) {
      const id = request.query.id;
      const response = status(id);
      return h.response(response);
    },
    requestStatusHttp: async function(request, h) {
      const iterations = request.query.i;
      // make an http request to the OTHER_API_URL 1000 times and record the time it takes
      console.time(`requestStatusHttp|${iterations}`);
      (async () => {
        for (let i = 0; i < iterations; i++) {
          const response = await httpGet(`http://${process.env.OTHER_API_URL}/status?id=${i}`);
          // console.log('received: %s', response);
        }
        console.timeEnd(`requestStatusHttp|${iterations}`);
      })();
      return "something";
    },




    getStatusWebsocket: async function(request, h) {
      let { initially, ws } = request.websocket();
      // code to execute when this connection is first opened
      if (initially) {
        ws.send(JSON.stringify({id: null, result: {status: 'welcome'}}));
      } else {
        // code to execute on subsequent messages
        if (request.payload.method === "getStatus") {
          ws.send(JSON.stringify({ id: request.payload.id, result: status(request.payload.id) }));
        }
      }

      return "";
    },
    requestStatusWebsocket: async function(request, h) {
      // could wrap this whole thing in a for loop and an anonymous function. might need to also wrap in a promise so we can await and resolve
      const iterations = request.query.i;
      // make a websocket request to the OTHER_API_URL 1000 times and record the time it takes
      console.time(`requestStatusWebsocket|${iterations}`);
      const ws = new WebSocket(`ws://${process.env.OTHER_API_URL}/ws/status`, {
        perMessageDeflate: false
      });

      ws.on('error', console.error);

      const responses = {};

      // promise that resolves when a function is called
      const laterPromise = () => {
        let resolve;
        const promise = new Promise((r) => {
          resolve = r;
        });
        promise.resolve = resolve;
        return promise;
      };

      ws.on('open', function open() {
        (async () => {

          for (let i=0; i< iterations; i++) {

            const id = i.toString();

            responses[id] = laterPromise();

            ws.send(JSON.stringify({ id: id, method: "getStatus", params: [{address: id + " Main St" }] }));

            await responses[id];
          }
          console.timeEnd(`requestStatusWebsocket|${iterations}`);
          //ws.terminate()
        })();
      });
  
      ws.on('message', function message(data) {
        const {id, result} = JSON.parse(data);
        if (responses.hasOwnProperty(id)) {
          responses[id].resolve(result);
        }
      });

      return "something";

    }
  };

  function status (id) {
    return {
      id,
      status: 'up'
    };
  }

};
