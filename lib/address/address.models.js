module.exports = {
  db: async (sequelize, DataTypes) => {
    const Address = sequelize.define('Address', {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      "ADDR_NUM": DataTypes.STRING,
      "ST_PREFIX": DataTypes.STRING,
      "ST_NAME": DataTypes.STRING,
      "ST_TYPE": DataTypes.STRING,
      "MUNICIPALITY": DataTypes.STRING,
      "COUNTY": DataTypes.STRING,
      "STATE": DataTypes.STRING,
      "ZIP_CODE": DataTypes.STRING,
      "ST_SUFFIX": DataTypes.STRING,
      geometry: { 
        type: DataTypes.GEOMETRY 
      },
    });

    return Address;
  }
};
