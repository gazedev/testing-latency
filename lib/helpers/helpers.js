module.exports = {
  httpGet: httpGet,
};

async function httpGet(getUrl) {
  const url = new URL(getUrl);

  let https;
  if (url.protocol == 'http:') {
    https = require('http');
  } else {
    https = require('https');
  }

  let promise = new Promise((resolve, reject) => {
    https.get(url, res => {
      res.setEncoding("utf8");
      let body = "";
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        try {
          body = JSON.parse(body);
        } catch (e) {
          // log the error, but don't throw here or we will crash the api
          console.log(e);         
        }
        resolve(body);
      });
      res.on('error', error => {
        reject(error);
      });
    });

  });

  return promise;
}